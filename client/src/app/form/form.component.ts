import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { WeightService } from '../weight.service';
import { UserService } from '../user.service';
import { tap, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  formGroup: FormGroup;

  userId: string;

  user$: Observable<any>;

  constructor(private _fb: FormBuilder,
    private _weightService: WeightService,
    private _router: Router,
    private _userService: UserService,
    private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.user$ = this._activatedRoute.params
      .pipe(
        tap(({ id }) => this.userId = id),
        mergeMap(({ id }) => this._userService.get(id))
      );

    this.formGroup = this._fb.group({
      weight: [null, Validators.required]
    });
  }

  send(): void {
    const data = this.formGroup.getRawValue();

    data.userId = this.userId;

    this._weightService.save(data)
      .subscribe(() => this._router.navigate(['user', this.userId]));
  }
}
