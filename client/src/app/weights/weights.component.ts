import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { WeightService } from '../weight.service';

@Component({
  selector: 'app-weights',
  templateUrl: './weights.component.html',
  styleUrls: ['./weights.component.css']
})
export class WeightsComponent implements OnInit {

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Dzień';
  showYAxisLabel = true;
  yAxisLabel = 'Waga';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // line, area
  autoScale = true;

  formGroup: FormGroup;

  userId: string;

  data$: Observable<any>;

  constructor(private _fb: FormBuilder,
    private _weightService: WeightService,
    private _activatedRoute: ActivatedRoute) { }

  formatDate(date: number): string {
    const dateObj = new Date(date);
    const month = dateObj.getUTCMonth() + 1;
    const day = dateObj.getUTCDate();
    const year = dateObj.getUTCFullYear();

    return year + '/' + month + '/' + day;
  }

  ngOnInit() {
    this.data$ = this._activatedRoute.params
      .pipe(
        tap(({ id }) => this.userId = id),
        mergeMap(({ id }) => this._weightService.get(id)),
        map(weights => weights.map(weight => ({ name: this.formatDate(weight.dateOfMeasure), value: weight.weight }))),
        map(series => ([{ name: 'Waga', series }]))
      );

    this.formGroup = this._fb.group({
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required]
    });
  }
}
