import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TableDataSource } from './table-datasource';
import { WeightService } from '../weight.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TableDataSource;

  displayedColumns = ['weight', 'dateOfMeasure'];

  @Input()
  filters;

  @Input()
  userId: string;

  constructor(private _weightService: WeightService) {
  }

  ngOnInit() {
    this.dataSource = new TableDataSource(this.paginator, this.sort, this.filters);

    this._weightService.get(this.userId)
      .subscribe(data => {
        this.dataSource.data = data;
        this.dataSource.source.next();
      });
  }
}
