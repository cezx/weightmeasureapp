import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private _fb: FormBuilder,
    private _router: Router,
    private _userService: UserService) { }

  ngOnInit() {
    this.formGroup = this._fb.group({
      name: [null, Validators.required],
      surname: [null, Validators.required],
      age: [null, Validators.required],
    });
  }

  send(): void {
    const data = this.formGroup.getRawValue();

    this._userService.save(data)
      .subscribe(() => this._router.navigateByUrl('/'));
  }
}
