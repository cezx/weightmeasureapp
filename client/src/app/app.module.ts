import { NgModule, LOCALE_ID } from '@angular/core';
import {
  MatButtonModule, MatCardModule, MatPaginatorModule,
  MatSortModule, MatTableModule, MatToolbarModule, MatInputModule,
  MatIconModule, MatDatepickerModule, MAT_DATE_LOCALE, MatNativeDateModule, MatListModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { ReactiveFormsModule } from '@angular/forms';
import localePl from '@angular/common/locales/pl';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { WeightsComponent } from './weights/weights.component';
import { UserComponent } from './user/user.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

registerLocaleData(localePl);

const MATERIAL_MODULES = [
  MatToolbarModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatIconModule,
  MatListModule,
  MatDatepickerModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TableComponent,
    FormComponent,
    WeightsComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxChartsModule,

    ...MATERIAL_MODULES,

    MatNativeDateModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'pl-PL'},
    {provide: LOCALE_ID, useValue: 'pl'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
