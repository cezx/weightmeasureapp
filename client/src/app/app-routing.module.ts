import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { WeightsComponent } from './weights/weights.component';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'form/:id',
        component: FormComponent
      },
      {
        path: 'user/:id',
        component: WeightsComponent
      }
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
