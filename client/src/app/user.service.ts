import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = environment.server + '/users';

  constructor(private _http: HttpClient) { }

  save(data: any): Observable<any> {
    return this._http.post(`${this.url}/save`, data);
  }

  getAll(): Observable<any> {
    return this._http.get(`${this.url}/all`);
  }

  get(id: string): Observable<any> {
    const params = new HttpParams()
      .set('id', id);

    return this._http.get(`${this.url}/get`, { params });
  }
}
