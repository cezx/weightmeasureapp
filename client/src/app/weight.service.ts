import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeightService {

  url = environment.server + '/weight';

  constructor(private _http: HttpClient) { }

  save(data: any): Observable<any> {
    return this._http.post(`${this.url}/save`, data);
  }

  get(userId: string): Observable<any> {
    return this._http.get(`${this.url}/get/${userId}`);
  }
}
