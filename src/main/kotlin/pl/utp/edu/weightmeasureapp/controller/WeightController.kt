package pl.utp.edu.weightmeasureapp.controller

import org.springframework.web.bind.annotation.*
import pl.utp.edu.weightmeasureapp.model.Weight
import pl.utp.edu.weightmeasureapp.service.WeightService

@RestController
@RequestMapping("/weight")
class WeightController(val weightService: WeightService) {

    @PostMapping("/save")
    fun saveWeight(@RequestBody weight: Weight){
        weightService.saveWeight(weight)
    }

    @GetMapping("/get/{userId}")
    fun getAllForUser(@PathVariable userId: String) =  weightService.getWeightsForUser(userId.toInt())
}