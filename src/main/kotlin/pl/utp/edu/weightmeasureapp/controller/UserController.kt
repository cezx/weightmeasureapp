package pl.utp.edu.weightmeasureapp.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.utp.edu.weightmeasureapp.model.UserModel
import pl.utp.edu.weightmeasureapp.service.UserService

@RestController
@RequestMapping("/users")
class UserController(val userService: UserService) {


    @PostMapping("/save")
    fun saveUser(@RequestBody userModel: UserModel) =
            userService.addUser(userModel)

    @GetMapping("/get")
    fun getUser(@RequestParam(required = false) id : String?,
                @RequestParam(required = false) name: String?,
                @RequestParam(required = false) surname: String?) : ResponseEntity<UserModel> {

        return if(id != null) {
            ResponseEntity.ok(userService.getUserById(id.toInt()))
        }else if(name != null && surname != null){
            ResponseEntity.ok(userService.getUserByName(name,surname))
        } else throw RuntimeException("Nie podano wystarczająco argumentów aby wyszukać użytkownika")
    }

    @GetMapping("/all")
    fun getAll() = userService.getAllUsers()




}