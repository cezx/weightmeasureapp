package pl.utp.edu.weightmeasureapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class WeightmeasureappApplication

fun main(args: Array<String>) {
    runApplication<WeightmeasureappApplication>(*args)
}
