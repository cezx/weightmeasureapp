package pl.utp.edu.weightmeasureapp.service

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import pl.utp.edu.weightmeasureapp.model.UserModel
import pl.utp.edu.weightmeasureapp.repository.UserRepository

@Service
class UserService(val userRepository: UserRepository) {

    fun addUser(userModel: UserModel) {
        try {
            userRepository.save(userModel)
        } catch (ex: DataIntegrityViolationException){
            val message = ex.cause?.cause!!.message!!
            throwIfMessageContainsUserData(message, userModel)
        }
    }

    fun getUserById(id: Int) = userRepository.findById(id).orElseThrow { RuntimeException("Nie znaleziono uzytkownka o id : $id") }

    fun getUserByName(name: String, surname : String) = userRepository.findUserModelByNameAndSurname(name,surname)
            .orElseThrow { RuntimeException("Nie znaleziono uzytkownika o imieniu : $name ,  i nazwisku : $surname") }

    fun getAllUsers() = userRepository.findAll()



    private fun throwIfMessageContainsUserData(message: String, userModel: UserModel) {
        if (message.contains("Duplicate entry", true) &&
                message.contains("${userModel.name}-${userModel.surname}", ignoreCase = true)) {
            throw RuntimeException("Użytkownik o imieniu '${userModel.name}' i nazwisku '${userModel.surname}' istnieje juz w db")
        }
    }
}