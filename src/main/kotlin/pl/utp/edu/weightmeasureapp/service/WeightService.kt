package pl.utp.edu.weightmeasureapp.service

import org.springframework.stereotype.Service
import pl.utp.edu.weightmeasureapp.model.Weight
import pl.utp.edu.weightmeasureapp.repository.WeightRepository

@Service
class WeightService(val weightRepository: WeightRepository) {

    fun saveWeight(weight: Weight) {
        weightRepository.save(weight)
    }

    fun getWeightsForUser(id : Int) = weightRepository.findAllByUserId(id)
}