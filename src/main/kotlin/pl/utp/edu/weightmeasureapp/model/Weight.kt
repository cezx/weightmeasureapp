package pl.utp.edu.weightmeasureapp.model

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "weight")
data class Weight(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id : Int? = null,

        @Column(name = "user_id")
        val userId : Int,
        val weight: Float,
        @Column(name = "date_of_measure")
        val dateOfMeasure: Long = Date().time
) {
        constructor() : this(null,0,0f,Date().time)
}