package pl.utp.edu.weightmeasureapp.model

import javax.persistence.*

@Entity
@Table(name = "users")
data class UserModel(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id : Int?,
        val name: String ,
        val surname: String,
        val age: Int
) {
        constructor() : this(null,"","",0)
}