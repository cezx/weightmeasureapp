package pl.utp.edu.weightmeasureapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.utp.edu.weightmeasureapp.model.Weight;

import java.util.List;

@Repository
public interface WeightRepository extends CrudRepository<Weight,Integer> {


    List<Weight> findAllByUserId(Integer userId);
}
