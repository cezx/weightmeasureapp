package pl.utp.edu.weightmeasureapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.utp.edu.weightmeasureapp.model.UserModel;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserModel,Integer> {

    Optional<UserModel> findUserModelByNameAndSurname(String name, String surname);
}
