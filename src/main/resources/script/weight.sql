CREATE TABLE weight(
  id INTEGER AUTO_INCREMENT PRIMARY KEY ,
  user_id INTEGER,
  weight FLOAT ,
  date_of_measure BIGINT
);